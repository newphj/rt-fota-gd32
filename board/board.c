#include <board.h>
#include <rtthread.h>
#include <rtdevice.h>

void rt_hw_board_init()
{
	extern void hw_board_init(char *clock_src, int32_t clock_src_freq,
							  int32_t clock_target_freq);

	/* NVIC Configuration */
#define NVIC_VTOR_MASK 0xFFFFFF80
#ifdef VECT_TAB_RAM
	/* Set the Vector Table base location at 0x10000000 */
	SCB->VTOR = (0x10000000 & NVIC_VTOR_MASK);
#else /* VECT_TAB_FLASH  */
	/* Set the Vector Table base location at 0x08000000 */
	SCB->VTOR = (0x08000000 & NVIC_VTOR_MASK);
#endif

	/* Heap initialization */
#ifdef BSP_USING_SDRAM
	rt_system_heap_init((void *)EXT_SDRAM_BEGIN, (void *)EXT_SDRAM_END);
#else
	rt_system_heap_init((void *)HEAP_BEGIN, (void *)HEAP_END);
#ifdef RT_USING_MEMHEAP_AS_HEAP
	static struct rt_memheap _heap1;
	rt_memheap_init(&_heap1, "heap1", (void *)RAM1_START,
					RAM1_END - RAM1_START);
#endif
#endif

	hw_board_init(BSP_CLOCK_SOURCE, BSP_CLOCK_SOURCE_FREQ_MHZ,
				  BSP_CLOCK_SYSTEM_FREQ_MHZ);

	/* Set the shell console output device */
#if defined(RT_USING_DEVICE) && defined(RT_USING_CONSOLE)
	rt_console_set_device(RT_CONSOLE_DEVICE_NAME);
#endif

	/* Board underlying hardware initialization */
#ifdef RT_USING_COMPONENTS_INIT
	rt_components_board_init();
#endif
}
