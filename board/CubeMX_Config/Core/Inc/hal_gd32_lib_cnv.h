/**
    ******************************************************************************
    * @file    hal_gd32_lib_cnv.h
    * @author  yangFei
    * @brief   将hal库转换为标准库的文件
    *
    * @version V1.0.0
    * @date    2022.05.24
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.05.24     yangFei
    *
    ******************************************************************************
    */
#ifndef _HAL_GD32_LIB_CNV_H 
#define _HAL_GD32_LIB_CNV_H 

#include "drv_gpio.h"


#define HAL_LIB 0 //0 - 不使用hal库

#if HAL_LIB

#else

#define GPIO_PIN_RESET       RESET
#define GPIO_PIN_SET         SET

#define HAL_GPIO_WritePin(GPIOx, GPIO_Pin, PinState)   gpio_bit_write(GPIOx, GPIO_Pin, PinState)
#define HAL_GPIO_ReadPin(GPIOx, GPIO_Pin)    gpio_input_bit_get(GPIOx, GPIO_Pin)

#define __HAL_RCC_GPIOA_CLK_ENABLE() rcu_periph_clock_enable(RCU_GPIOA)  
#define __HAL_RCC_GPIOB_CLK_ENABLE() rcu_periph_clock_enable(RCU_GPIOB)

#define HAL_GPIO_DeInit(GPIOx, GPIO_Pin)   __NOP  //gpio_deinit(GPIOx)  gd32没有初始化单个引脚，只有port

#define HAL_Delay(Delay)   rt_thread_mdelay(Delay) 

#define UID_BASE              (0x1FFF7A18UL)        /*!< Unique device ID register base address */
#define READ_REG(REG)         ((REG))
#define HAL_GetUIDw0          (READ_REG(*((uint32_t *)UID_BASE)))
#define HAL_GetUIDw1          (READ_REG(*((uint32_t *)(UID_BASE + 4U))))
#define HAL_GetUIDw2          (READ_REG(*((uint32_t *)(UID_BASE + 8U))))

#define  WWDG_IRQn             0     // Window WatchDog Interrupt 
#endif

#endif /* _HAL_GD32_LIB_CNV_H */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/


