/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
//#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#if 0
/* USER CODE END EM */

void GD32_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
#endif
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CS1_Pin GPIO_PIN_13
#define CS1_GPIO_Port GPIOC
#define CS2_Pin GPIO_PIN_0
#define CS2_GPIO_Port GPIOC
#define CS3_Pin GPIO_PIN_3
#define CS3_GPIO_Port GPIOC
#define TX_DET_Pin GPIO_PIN_2
#define TX_DET_GPIO_Port GPIOC
#define ANT_DET_Pin GPIO_PIN_1
#define ANT_DET_GPIO_Port GPIOC
#define V_TEMP_Pin GPIO_PIN_5
#define V_TEMP_GPIO_Port GPIOC
#define PA_VBIAS_Pin GPIO_PIN_4
#define PA_VBIAS_GPIO_Port GPIOA
#define PA_ENABLE_Pin GPIO_PIN_5
#define PA_ENABLE_GPIO_Port GPIOA
#define BT_UART_CTS_Pin GPIO_PIN_14
#define BT_UART_CTS_GPIO_Port GPIOB
#define BAT_SIG_Pin GPIO_PIN_0
#define BAT_SIG_GPIO_Port GPIOB
#define BT_UART_RTS_Pin GPIO_PIN_13
#define BT_UART_RTS_GPIO_Port GPIOB
#define BT_RESET_Pin GPIO_PIN_2
#define BT_RESET_GPIO_Port GPIOB
#define BT_UART_RX_Pin GPIO_PIN_10
#define BT_UART_RX_GPIO_Port GPIOB
#define BT_UART_TX_Pin GPIO_PIN_11
#define BT_UART_TX_GPIO_Port GPIOB
#define RF_PWR_OFF_Pin GPIO_PIN_12
#define RF_PWR_OFF_GPIO_Port GPIOB
#define I2C_SCL_Pin GPIO_PIN_6
#define I2C_SCL_GPIO_Port GPIOB
#define I2C_SDA_Pin GPIO_PIN_7
#define I2C_SDA_GPIO_Port GPIOB
#define IMU_INT1_Pin GPIO_PIN_15
#define IMU_INT1_GPIO_Port GPIOB
#define IMU_INT1_EXTI_IRQn EXTI15_10_IRQn
#define VIB_Pin GPIO_PIN_6
#define VIB_GPIO_Port GPIOC
#define LED_R_Pin GPIO_PIN_7
#define LED_R_GPIO_Port GPIOC
#define LED_G_Pin GPIO_PIN_8
#define LED_G_GPIO_Port GPIOC
#define LED_B_Pin GPIO_PIN_9
#define LED_B_GPIO_Port GPIOC
#define POWER_ON_Pin GPIO_PIN_8
#define POWER_ON_GPIO_Port GPIOA
#define FLASH_CS_Pin GPIO_PIN_15
#define FLASH_CS_GPIO_Port GPIOA
#define KEY_DOWN_Pin GPIO_PIN_2
#define KEY_DOWN_GPIO_Port GPIOD
#define KEY_UP_Pin GPIO_PIN_1
#define KEY_UP_GPIO_Port GPIOB
#define CS_3993_Pin GPIO_PIN_6
#define CS_3993_GPIO_Port GPIOA
#define IRQ_3993_Pin GPIO_PIN_8
#define IRQ_3993_GPIO_Port GPIOB
#define IRQ_3993_EXTI_IRQn EXTI9_5_IRQn
#define EN_3993_Pin GPIO_PIN_9
#define EN_3993_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
