/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-20     BruceOu      first implementation
 */

#ifndef __DRV_SPI_H__

#define __DRV_SPI_H__

#include <rthw.h>
#include <rtthread.h>
#include <board.h>

#ifdef __cplusplus
extern "C"
{
#endif

struct gd32_spi_cs
{
	uint32_t GPIOx;
	uint32_t GPIO_Pin;
};

typedef struct
{
	rcu_periph_enum rcu_clk;
	uint32_t port;
	uint32_t pin;
} spi_port_t;

/* gd32 spi dirver class */
struct gd32_spi
{
	uint32_t spi_periph;
	char *bus_name;
	rcu_periph_enum spi_rcu_clk;
	struct rt_spi_bus *spi_bus;
#if defined SOC_SERIES_GD32F4xx
	uint32_t alt_func_num;
#endif
	spi_port_t sck;
	spi_port_t miso;
	spi_port_t mosi;
};

rt_err_t rt_hw_spi_device_attach(const char *bus_name, const char *device_name,
		uint32_t cs_gpiox, uint16_t cs_gpio_pin);

#ifdef __cplusplus
}
#endif

#endif /* __DRV_SPI_H__ */
