/**
    ******************************************************************************
    * @file    drv_flash.c
    * @author  yangFei
    * @brief   按照GD32库实现flash驱动
    *
    * @version V1.0.0
    * @date    2022.06.08
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.06.08     yangFei
    *
    ******************************************************************************
    */
		
#ifndef _DRV_FLASH_H 
#define _DRV_FLASH_H 

#include "gd32f4xx_fmc.h"
#include "fmc_operation.h"
#include "board.h"



#endif /* _DRV_FLASH_H */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/


