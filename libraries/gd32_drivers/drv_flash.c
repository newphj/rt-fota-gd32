/**
    ******************************************************************************
    * @file    drv_flash.c
    * @author  yangFei
    * @brief   ����GD32��ʵ��flash����
    *
    * @version V1.0.0
    * @date    2022.06.08
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.06.08     yangFei
    *
    ******************************************************************************
    */

#include <board.h>

#ifdef BSP_USING_ON_CHIP_FLASH
#include "drv_flash.h"


#if defined(PKG_USING_FAL)
#include "fal.h"
#endif

//#define DRV_DEBUG
#define LOG_TAG                "drv.flash"
#include <drv_log.h>


#if defined(PKG_USING_FAL) 

static int fal_flash_read(long offset, rt_uint8_t *buf, size_t size);
static int fal_flash_write(long offset, const rt_uint8_t *buf, size_t size);
static int fal_flash_erase(long offset, size_t size);

const struct fal_flash_dev gd32_onchip_flash = 
	{ "onchip_flash", 
	  GD32_FLASH_START_ADRESS, 
	  GD32_FLASH_SIZE, 
	  SIZE_4KB, 
	  {NULL, fal_flash_read, fal_flash_write, fal_flash_erase}};

static int fal_flash_read(long offset, rt_uint8_t *buf, size_t size)
{
	  return fmc_read_8bit_data(gd32_onchip_flash.addr + offset, size, (int8_t*)buf);
}

static int fal_flash_write(long offset, const rt_uint8_t *buf, size_t size)
{
	  return fmc_write_8bit_data(gd32_onchip_flash.addr + offset, size, (int8_t*)buf);
}

static int fal_flash_erase(long offset, size_t size)
{
	   fmc_sector_info_struct   page;
	
		  if ((gd32_onchip_flash.addr + offset + size) > GD32_FLASH_END_ADDRESS)
			{
					LOG_E("ERROR: erase outrange flash size! addr is (0x%p)\n", (void*)(gd32_onchip_flash.addr + offset + size));
					return -RT_EINVAL;
			}
			
			fmc_unlock(); 
			/* clear pending flags */
			fmc_flag_clear(FMC_FLAG_END | FMC_FLAG_OPERR | FMC_FLAG_WPERR | FMC_FLAG_PGMERR | FMC_FLAG_PGSERR);
			/* Get the 1st page to erase */
			page = fmc_sector_info_get(gd32_onchip_flash.addr + offset);
			/* Get the number of pages to erase from 1st page */
			page = fmc_sector_info_get(gd32_onchip_flash.addr + offset + size - 1);
			page.sector_end_addr += 1;
			//    /* erase page */
//			for(uint32_t i = page.sector_start_addr; i < page.sector_end_addr; i++)
//			{
					page = fmc_erase_sector_by_address(page.sector_start_addr);
				  /* wait the erase operation complete*/
//					while(FMC_READY != page.sector_num)
//					{
//							;
//					}
//			}
			
			/* lock the flash program erase controller */
			fmc_lock();

		
    return size;
}

#endif

#endif /* BSP_USING_ON_CHIP_FLASH */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/



