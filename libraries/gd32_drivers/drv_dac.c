/**
    ******************************************************************************
    * @file    drv_dac.c
    * @author  yangFei
    * @brief   ����GD32��ʵ��dac����
    *
    * @version V1.0.0
    * @date    2022.06.06
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.06.06     yangFei
    *
    ******************************************************************************
    */

#include <board.h>

#if defined(BSP_USING_DAC0) || defined(BSP_USING_DAC1)
#include "drv_dac.h"
#include "dac_config.h"

//#define DRV_DEBUG
#define LOG_TAG             "drv.dac"
#include <drv_log.h>


static DAC_HandleTypeDef dac_config[] =
{
#ifdef BSP_USING_DAC0
    DAC0_CONFIG,
#endif
    
#ifdef BSP_USING_DAC1
    DAC1_CONFIG,
#endif
};

struct gd32_dac
{
    DAC_HandleTypeDef DAC_Handler;
    struct rt_dac_device gd32_dac_device;
};

static struct gd32_dac gd32_dac_obj[sizeof(dac_config) / sizeof(dac_config[0])];

static rt_err_t gd32_dac_enabled(struct rt_dac_device *device, rt_uint32_t channel)
{
    DAC_HandleTypeDef *gd32_dac_handler;
    RT_ASSERT(device != RT_NULL);
    gd32_dac_handler = device->parent.user_data;

#if defined(SOC_SERIES_GD32F4xx)
        dac_enable(gd32_dac_handler->Instance); 
#endif
    
    return RT_EOK;
}

static rt_err_t gd32_dac_disabled(struct rt_dac_device *device, rt_uint32_t channel)
{
    DAC_HandleTypeDef *gd32_dac_handler;
    RT_ASSERT(device != RT_NULL);
    gd32_dac_handler = device->parent.user_data;
    
#if defined(SOC_SERIES_GD32F4xx)
    dac_disable(gd32_dac_handler->Instance);
#endif
    
    return RT_EOK;
}


static rt_err_t gd32_set_dac_value(struct rt_dac_device *device, rt_uint32_t channel, rt_uint32_t *value)
{
    uint32_t dac_channel;	
    DAC_HandleTypeDef *gd32_dac_handler;
   
    RT_ASSERT(device != RT_NULL);
    RT_ASSERT(value != RT_NULL);

    gd32_dac_handler = device->parent.user_data;
    
    /* config dac out channel*/
    
    /* set dac channel out value*/
		dac_data_set(gd32_dac_handler->Instance, DAC_ALIGN_12B_R, *value);
    /* start dac */
    dac_enable(gd32_dac_handler->Instance);
  
    return RT_EOK;
}

static const struct rt_dac_ops stm_dac_ops =
{
    .disabled = gd32_dac_disabled,
    .enabled  = gd32_dac_enabled,
    .convert  = gd32_set_dac_value,
};

static int gd32_dac_init(void)
{
    int result = RT_EOK;
    /* save dac name */
    char name_buf[5] = {'d', 'a', 'c', '0', 0};
    int i = 0;
		
		rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_DAC);
    dac_deinit();
		
    for (i = 0; i < sizeof(dac_config) / sizeof(dac_config[0]); i++)
    {
        /* dac init */
        name_buf[3] = '0';
        gd32_dac_obj[i].DAC_Handler = dac_config[i];
			
			  dac_trigger_disable(gd32_dac_obj[i].DAC_Handler.Instance);
			  dac_wave_mode_config(gd32_dac_obj[i].DAC_Handler.Instance, DAC_WAVE_DISABLE);
			  dac_output_buffer_enable(gd32_dac_obj[i].DAC_Handler.Instance);
			
#if defined(DAC0)
        if (gd32_dac_obj[i].DAC_Handler.Instance == DAC0)
        {
            name_buf[3] = '0';
						gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_4);
        }
#endif
#if defined(DAC1)
        if (gd32_dac_obj[i].DAC_Handler.Instance == DAC1)
        {
            name_buf[3] = '1';
					  gpio_mode_set(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO_PIN_5);
        }
#endif
				

				/* register dac device */
				if (rt_hw_dac_register(&gd32_dac_obj[i].gd32_dac_device, name_buf, &stm_dac_ops, &gd32_dac_obj[i].DAC_Handler) == RT_EOK)
				{
						LOG_D("%s init success", name_buf);
				}
				else
				{
						LOG_E("%s register failed", name_buf);
						result = -RT_ERROR;
				}
    }

    return result;
}
INIT_DEVICE_EXPORT(gd32_dac_init);

#endif /* BSP_USING_DAC */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/
