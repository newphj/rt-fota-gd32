/**
    ******************************************************************************
    * @file    drv_pwm.h
    * @author  yangFei
    * @brief   存放hal库中关于pwm相关的定义
    *
    * @version V1.0.0
    * @date    2022.05.30
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.05.30     yangFei
    *
    ******************************************************************************
    */
#ifndef _DRV_PWM_H 
#define _DRV_PWM_H 

#include "gd32f4xx_timer.h"
#include "board.h"


/**
  * @brief  HAL State structures definition
  */
typedef enum
{
  HAL_TIM_STATE_RESET             = 0x00U,    /*!< Peripheral not yet initialized or disabled  */
  HAL_TIM_STATE_READY             = 0x01U,    /*!< Peripheral Initialized and ready for use    */
  HAL_TIM_STATE_BUSY              = 0x02U,    /*!< An internal process is ongoing              */
  HAL_TIM_STATE_TIMEOUT           = 0x03U,    /*!< Timeout state                               */
  HAL_TIM_STATE_ERROR             = 0x04U     /*!< Reception process is ongoing                */
} HAL_TIM_StateTypeDef;

/**
  * @brief  HAL Active channel structures definition
  */
typedef enum
{
  HAL_TIM_ACTIVE_CHANNEL_0        = TIMER_CH_0,    /*!< The active channel is 0     */
	HAL_TIM_ACTIVE_CHANNEL_1        = TIMER_CH_1,    /*!< The active channel is 1     */
  HAL_TIM_ACTIVE_CHANNEL_2        = TIMER_CH_2,    /*!< The active channel is 2     */
  HAL_TIM_ACTIVE_CHANNEL_3        = TIMER_CH_3    /*!< The active channel is 3     */
} HAL_TIM_ActiveChannel;

/**
  * @brief  TIM Time Base Handle Structure definition
  */
typedef struct
{
  uint32_t                     Instance;     /*!< Register base address             */
  timer_parameter_struct        Init;          /*!< TIM Time Base required parameters */
  HAL_TIM_ActiveChannel       Channel;       /*!< Active channel                    */
//  DMA_HandleTypeDef           *hdma[7];      /*!< DMA Handlers array
//                                                  This array is accessed by a @ref DMA_Handle_index */
  HAL_LockTypeDef             Lock;          /*!< Locking object                    */
  __IO HAL_TIM_StateTypeDef   State;         /*!< TIM operation state               */
	
} TIM_HandleTypeDef;

#endif /* _DRV_PWM_H */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/


