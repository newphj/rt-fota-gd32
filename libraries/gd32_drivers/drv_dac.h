/**
    ******************************************************************************
    * @file    drv_dac.h
    * @author  yangFei
    * @brief   按照GD32库实现dac驱动
    *
    * @version V1.0.0
    * @date    2022.06.06
    * @verbatim
    *
    * @endverbatim
    ******************************************************************************
    * @note
    * @attention
    *
    * Change Logs:
    * Date           Author       Notes
    * 2022.06.06     yangFei
    *
    ******************************************************************************
    */
		
#ifndef _DRV_DAC_H 
#define _DRV_DAC_H 

#include "gd32f4xx_timer.h"
#include "board.h"

#define DAC_CHANNEL_0                      0x00000000U
#define DAC_CHANNEL_1                      0x00000010U
/**
  * @brief  HAL State structures definition
  */
typedef enum
{
  HAL_DAC_STATE_RESET             = 0x00U,  /*!< DAC not yet initialized or disabled  */
  HAL_DAC_STATE_READY             = 0x01U,  /*!< DAC initialized and ready for use    */
  HAL_DAC_STATE_BUSY              = 0x02U,  /*!< DAC internal processing is ongoing   */
  HAL_DAC_STATE_TIMEOUT           = 0x03U,  /*!< DAC timeout state                    */
  HAL_DAC_STATE_ERROR             = 0x04U   /*!< DAC error state                      */

} HAL_DAC_StateTypeDef;


/**
  * @brief  DAC handle Structure definition
  */
typedef struct
{
  uint32_t                 Instance;     /*!< Register base address             */

  __IO HAL_DAC_StateTypeDef   State;         /*!< DAC communication state           */

  HAL_LockTypeDef             Lock;          /*!< DAC locking object                */

//  DMA_HandleTypeDef           *DMA_Handle1;  /*!< Pointer DMA handler for channel 1 */

//  DMA_HandleTypeDef           *DMA_Handle2;  /*!< Pointer DMA handler for channel 2 */

  __IO uint32_t               ErrorCode;     /*!< DAC Error code                    */


} DAC_HandleTypeDef;


#endif /* _DRV_DAC_H */

/************************ (C) COPYRIGHT LEADINNO *****END OF FILE****/


