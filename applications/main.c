/**
  ******************************************************************************
  * @file    main.c
  * @author  Taoking
  * @brief Remark_the_file_introduction______it_is_block_comment_
  *          This is the common part of the file
  * @version V0.0
  * @since   2021-01-01
  * @verbatim
  * Detailed_description_of_the_document
  * @endverbatim
  ******************************************************************************
  * @note CopyRight By XXXX Co. Ltd All right reserve
  * @attention
  *
  ******************************************************************************
  */
#include <rtthread.h>
#include <rt_fota.h>

/* 添加 fal 头文件 */
#include <fal.h>
/* 添加文件系统头文件 */


#if defined(RT_USING_FINSH) && defined(FINSH_USING_MSH)
#include <finsh.h>
#include <shell.h>
#endif





/* defined the LED pin: PC6 */
#define LED_PIN GET_PIN(C, 8)

#define DBG_ENABLE
#define DBG_SECTION_NAME                    "main"

#ifdef RT_MAIN_DEBUG
#define DBG_LEVEL                           DBG_LOG
#else
#define DBG_LEVEL                           DBG_INFO
#endif

#define DBG_COLOR
#include <rtdbg.h>

const char boot_log_buf[] = 
" ____ _____      _____ ___  _____  _        \r\n\
|  _ \\_   _|	|  ___/ _ \\ _   _|/ \\      \r\n\
| |_) || |_____ | |_  || ||  | | / _ \\      \r\n\
|  _ < | |_____ |  _| ||_||  | |/ ___ \\     \r\n\
|_| \\_\\|_|	|_|   \\___/  |_/_/   \\_\\  \r\n";

void rt_fota_print_log(void)
{   
  
	  LOG_RAW("2016 - 2019 Copyright by Radiation @ warfalcon \r\n");
	  LOG_RAW("Version: %s build %s\r\n\r\n", RT_FOTA_SW_VERSION, __DATE__);
}
 
int main(void)
{    

   

#if defined(RT_USING_FINSH) && defined(FINSH_USING_MSH)
	finsh_set_prompt("rt-fota />");
#endif
	
	    extern void rt_fota_init(void);
      rt_fota_init();
	

    return RT_EOK;
}


#include <rtthread.h>
#include "fal.h"
#include "spi_flash_sfud.h"
#include "drv_spi.h"


static int rt_spi_port_init(void)
{

	int ret;
    ret = rt_hw_spi_device_attach("spi1", "NorFlash", GPIOA, GPIO_PIN_3);

	if(ret == RT_EOK)
	{
		if (RT_NULL == rt_sfud_flash_probe(NOR_FLASH_DEV_NAME,"NorFlash"))
		{
			LOG_E("Flash probe failed %d\n");
			return -RT_ERROR;
		};
	}

    return RT_EOK;
}
INIT_DEVICE_EXPORT(rt_spi_port_init);
