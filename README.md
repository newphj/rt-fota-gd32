# 基于 GD32 的开源 Bootloader 框架 - RT-FOTA

移植自开源仓库https://gitee.com/spunky_973/rt-fota

适配GD32F42X系列，添加hash校验，修改为两个按键同时按下进入恢复工厂模式，修改片外升级为片内升级，进一步缩小bootloader大小,添加Kconfig文件，可以使用ENV进行配置

移植过程注意事项及其它外设移植细节参见 [移植完整版RT-Thread到GD32F4XX(详细)](https://yangfei.blog.csdn.net/article/details/125339296?spm=1001.2014.3001.5502)  中关于bootloader及flash移植相关内容，移植完成后使用及验证方法同stm32

